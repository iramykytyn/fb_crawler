# README #

# To get your application up and running you need to perform next steps:
#	1. Open terminal
#	2. Check if python is installed and what is current version:
		python --version
#	3. If you haven't python version 3.5 install it from here: https://www.python.org/ftp/python/3.6.2/python-3.6.2-macosx10.6.pkg
#	4. Install pip3 and type from terminal 
		pip3 install requests
#	5. Create directory facebook_pages and go there:
		mkdir ~/facebook_pages
		cd ~/facebook_pages
#	6. Put script to this directory
#	7. Run crawler typing: 
		python3.5 lets_crawl.py -page="fortafyfans"
		python3.5 lets_crawl.py -page="fortafyfans" -since="yyyy-mm-dd HH:MM:SS" -until="yyyy-mm-dd HH:MM:SS"
		python3.5 lets_crawl.py -page="fortafyfans" -since="yyyy-mm-dd HH:MM:SS"
		python3.5 lets_crawl.py -page="fortafyfans" -until="yyyy-mm-dd HH:MM:SS"
		
#	8. You will see how script runs and after it finish you will see csv file called like fb page, all scrapped data should be there.
