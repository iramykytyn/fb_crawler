import argparse
import requests, time, os
from datetime import datetime, timedelta
import csv
from requests.exceptions import Timeout, ConnectionError
from requests.packages.urllib3.exceptions import ReadTimeoutError
import random
import ssl

# facebook authentication staff
app_id = '310753286028733'
app_secret = '883fa843d99a504d1288016b3c5832f0'
token = 'access_token=' + app_id + '|' + app_secret
facebook_url = 'https://www.facebook.com/'
fb_graph_api_url = 'https://graph.facebook.com/v2.7/'

table_scheme = ['Post url', 'Created', 'Content link',
                'Likes', 'Shares', 'Is shared',
                'Post owner', 'Email', 'Fb link',
                'Website link', 'Number of fans', 'About']

table_row_dict = {'Post url': '',
                  'Created': '',
                  'Content link': '',
                  'Likes': 0,
                  'Shares': 0,
                  'Is shared': True,
                  'Post owner': '',
                  'Email': '',
                  'Fb link': '',
                  'Website link': '',
                  'Number of fans': 0,
                  'About': ''}


def randomSleep():
    sleeptime = random.randint(60, 120)
    time.sleep(sleeptime)


def getRequests(url):
    retry = 5
    while retry > 0:
        try:
            requests_result = requests.get(url, headers={'Connection': 'close'}).json()
            print(url)
            time.sleep(0.01)
            break
        except (Timeout, ssl.SSLError, ReadTimeoutError, ConnectionError) as e:
            print('url error:', e)
            randomSleep()
            retry -= 1
            continue
        except ValueError as e:  # includes simplejson.decoder.JSONDecodeError
            print('Decoding JSON has failed')
            print('url error:', e)
            retry = 0
            break

    if retry <= 0:
        print("Finishing loop with retry <= 0")
        return {}

    return requests_result


##########################################################################################################
page_info_dict = {
    'name': '',
    'link': '',
    'fan_count': 0,
    'emails': [],
    'website': '',
    'description': '',
}


def getInfoAboutPage(page_name, page_info):
    page_url = fb_graph_api_url + page_name
    info = getRequests(page_url + "/?fields=name,emails,fan_count,website,about,link&" + token)

    page_info['name'] = info['name'] if 'name' in info else ''
    page_info['link'] = info['link'] if 'link' in info else ''
    page_info['fan_count'] = info['fan_count'] if 'fan_count' in info else ''
    page_info['emails'] = info['emails'] if 'emails' in info else []
    page_info['website'] = info['website'] if 'website' in info else ''
    page_info['description'] = info['about'] if 'about' in info else ''

    print("Link " + str(page_info['link']))
    print("Fan count is " + str(page_info['fan_count']))
    print("Description: " + page_info['description'])

    return page_info


##########################################################################################################
post_info_dict = {
    'link': '',
    'created_time': '',
    'from': '',
    'permalink_url': '',
    'message': '',
    'message_tag': {},
    'description': '',
    'shares': 0,
    'likes': 0,
    'credited_list': []
}


def getLikesCount(post_id):
    likes = getRequests(fb_graph_api_url + post_id + "/likes?summary=true&" + token)
    count = likes['summary']['total_count'] if 'summary' in likes else 0
    return count


def getPostInfo(feed_url, post_info):
    info = getRequests(feed_url + "/?fields=created_time,description,from,link,permalink_url,"
                                  "message,message_tags,story,story_tags,shares&" + token)

    post_info_list = []

    # check if post isn't posted by fans
    if 'id' in info and len(info['id'].split('_')) >= 1:
        user_id = info['id'].split('_')[0]
        if not info['from']['id'] == user_id:
            return post_info_list

    post_info['Post url'] = info['permalink_url'] if 'permalink_url' in info else ''
    post_info['Content link'] = info['link'] if 'link' in info else ''
    post_info['Shares'] = info['shares']['count'] if 'shares' in info else 0
    print('Shares number: ' + str(post_info['Shares']))

    if 'created_time' in info:
        time_str = info['created_time'] if 'created_time' in info else ''
        # time to EST time
        date = datetime.strptime(time_str, "%Y-%m-%dT%H:%M:%S+0000")
        date = date + timedelta(hours=-4)
        post_info['Created'] = date.strftime("%m-%d-%Y %H:%M:%S")
        print('Created time: ' + post_info['Created'])


    if 'link' in info and len(post_info['Post url'].split('/')) >= 5:
        post_link_id = post_info['Post url'].split('/')[5]
        post_info['Likes'] = getLikesCount(post_link_id)

    hasShared = False
    hasTagged = False

    # check if post is shared from another page
    if len(post_info['Content link'].split('/')) >= 2 \
            and not post_info['Content link'].split('/')[3] == post_info['Post url'].split('/')[3] \
            and post_info['Content link'].split('/')[2] == facebook_url.split('/')[2]:
        post_info['Is shared'] = True
        person_name = post_info['Content link'].split('/')[3]
        credited_person_info = getInfoAboutPage(person_name, page_info)
        post_info['Post owner'] = credited_person_info['name']
        post_info['Fb link'] = credited_person_info['link']
        post_info['Email'] = credited_person_info['emails'][0] if credited_person_info['emails'] else ''
        post_info['Website link'] = credited_person_info['website']
        post_info['Number of fans'] = credited_person_info['fan_count']
        post_info['About'] = credited_person_info['description']

        hasShared = True
        post_info_list.append(post_info.copy())

    # check if post credit somebody
    message_tags_list = info['message_tags'] if 'message_tags' in info else []
    story_tags_list = info['story_tags'] if 'story_tags' in info else []
    for tag in story_tags_list:
        if tag['name'] == info['from']['name']:
            story_tags_list.remove(tag)

    if not post_info['Is shared']:
        tags_list = message_tags_list + story_tags_list
    else:
        tags_list = message_tags_list

    for credited_person in tags_list:
        if 'type' in credited_person and credited_person['type'] == 'page':
            post_info['Is shared'] = False
            credited_person_info = getInfoAboutPage(credited_person['id'], page_info)
            post_info['Post owner'] = credited_person_info['name']
            post_info['Fb link'] = credited_person_info['link']
            post_info['Email'] = credited_person_info['emails'][0] if credited_person_info['emails'] else ''
            post_info['Website link'] = credited_person_info['website']
            post_info['Number of fans'] = credited_person_info['fan_count']
            post_info['About'] = credited_person_info['description']
            post_info_list.append(post_info.copy())
            hasTagged = True

    if not (hasShared or hasTagged):
        message = info['message'] if 'message' in info else ''
        if 'Credit' in message \
                or 'credit' in message \
                or 'by' in message \
                or 'By' in message:
            print("Has credited person without link:", message)
            return {}

    return post_info_list


##########################################################################################################
def getFeedIds(feeds, feed_list):
    feeds = feeds['feed'] if 'feed' in feeds else feeds

    for feed in feeds['data']:
        feed_list.append(feed['id'])

    if 'paging' in feeds and 'next' in feeds['paging']:
        feeds_url = feeds['paging']['next']
        feed_list = getFeedIds(getRequests(feeds_url), feed_list)

    return feed_list


def cretePageInfoFile(page_name):
    try:
        file_name = page_name + '.csv'
        if not os.path.isfile(file_name):
            with open(file_name, 'a') as csvfile:
                writer = csv.writer(csvfile)
                writer.writerow(table_scheme)
    except IOError:
        print("I/O error({0}): {1}")
    return


def addPageInfoToFile(page_name, post_info):
    try:
        file_name = page_name + '.csv'
        if os.path.isfile(file_name):
            with open(file_name, 'a') as csvfile:
                writer = csv.writer(csvfile)

                table_cels_list = [post_info['Post url'],
                                   post_info['Created'],
                                   post_info['Content link'],
                                   str(post_info['Likes']),
                                   str(post_info['Shares']),
                                   str(post_info['Is shared']),
                                   post_info['Post owner'],
                                   post_info['Email'],
                                   post_info['Fb link'],
                                   post_info['Website link'],
                                   str(post_info['Number of fans']),
                                   post_info['About']
                                   ]

                writer.writerow(table_cels_list)
    except IOError:
        print("I/O error({0}): {1}")
    return


if __name__ == '__main__':

    # Set crawler target and parameters.
    parser = argparse.ArgumentParser()
    parser.add_argument("-page", help="Set the target page you want to crawling. Ex: 'fortafyfans '")
    parser.add_argument("-since", help="Set the start date you want to crawling. Format: 'yyyy-mm-dd HH:MM:SS'")
    parser.add_argument("-until", help="Set the end date you want to crawling. Format: 'yyyy-mm-dd HH:MM:SS'")
    args = parser.parse_args()

    target = str(args.page)

    # create log folder
    # target_dir = target + '/'
    # if not os.path.exists(target_dir):
    #     os.makedirs(target_dir)
    # os.chdir(target_dir)
    # log = open('log', 'w')
    start_time = datetime.now()

    print('Task start at:' + datetime.strftime(start_time, '%Y-%m-%d %H:%M:%S') + '\nTarget: ' + target + '\n')
    # log.write('Task start at:' + datetime.strftime(start_time, '%Y-%m-%d %H:%M:%S') + '\nTaget: ' + target + '\n')
    # log.close()

    # get contact info
    page_info = getInfoAboutPage(target, page_info_dict)

    # create csv file if not created yet
    cretePageInfoFile(target)

    # get all posts
    page_url = fb_graph_api_url + target
    feeds_url = page_url + '/?fields=feed.limit(100)'
    if args.since:
        since = str(args.since)
        feeds_url = feeds_url + '.since(' + since + ')'
    if args.until:
        until = str(args.until)
        feeds_url = feeds_url + '.until(' + until + ')'
    feeds_url = feeds_url + '{id}&' + token

    # Get list of feed id from target.
    feeds = getRequests(feeds_url)
    if not 'feed' in feeds:
        print('No post found.')
        exit(0)

    feed_list = getFeedIds(feeds, [])

    # scrape data from all posts
    post_number = 0
    post_with_credited_people = 0
    for feed_id in feed_list:
        post_number += 1
        feed_url = fb_graph_api_url + feed_id
        print(str(post_number) + ': parsing ' + feed_url)
        posts_info_list = getPostInfo(feed_url, table_row_dict)

        if not posts_info_list:
            print('Post has not credited people or shared content.')
        else:
            post_with_credited_people += 1

        for post_info in posts_info_list:
            print(post_info)
            addPageInfoToFile(target, post_info)

    print('\nTask finish at:' + datetime.strftime(start_time, '%Y-%m-%d %H:%M:%S') +
          '\nCrawled ' + str(feed_list.__len__()) + ' posts for target: ' + target +
          '\nFound ' + str(post_with_credited_people) + ' shared posts or posts with credited people.\n')

